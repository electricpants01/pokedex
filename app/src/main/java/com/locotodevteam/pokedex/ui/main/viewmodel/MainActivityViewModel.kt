package com.locotodevteam.pokedex.ui.main.viewmodel

import androidx.lifecycle.ViewModel

class MainActivityViewModel: ViewModel() {

    companion object{
        val instance = MainActivityViewModel()
    }
}