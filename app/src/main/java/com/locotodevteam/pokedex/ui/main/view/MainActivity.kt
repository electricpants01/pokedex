package com.locotodevteam.pokedex.ui.main.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.locotodevteam.pokedex.R
import com.locotodevteam.pokedex.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    lateinit var mainBinding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mainBinding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(mainBinding.root)
    }
}